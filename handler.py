import os
import requests
from bs4 import BeautifulSoup

def handle(req):
    """handle a request to the function
    Args:
        req (str): request body
    """
    s = os.environ.get("Http_Query")
    qp  = dict(item.split("=") for item in s.split(";"))
    print(qp)

    if qp.get("url")!=None:
        # url is present
        # BS
        url = qp["url"]
        get_url = requests.get(url)
        get_text = get_url.text
        soup = BeautifulSoup(get_text,"html.parser")
        c = soup.select("h1")
        return c

